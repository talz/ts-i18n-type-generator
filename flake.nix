{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let 
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        defaultPackage = pkgs.python3Packages.buildPythonPackage {
          pname = "generate_i18n_ts_types";
          version = "0.1.0";

          src = ./.;

          propagatedBuildInputs = with pkgs.python3Packages; [
            pyyaml
          ];
        };
      }
    );
}
