import argparse
import glob
import os.path
import yaml
import typing
import sys


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('locales_dir')
    parser.add_argument('--out', type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()

    namespaces = find_namespaces(args.locales_dir)

    namespace_to_common_keys = {
        namespace: find_common_keys(namespace, args.locales_dir)
        for namespace in namespaces
    }

    write_ns_interface(namespace_to_common_keys, args.out)


def write_ns_interface(namespace_to_common_keys, out: typing.TextIO):
    quote = "'"

    def string_type(options, indent_size):
        indent = ' ' * indent_size
        return f' |\n{indent}'.join(
            f'{quote}{option}{quote}' for option in sorted(options)
        )

    namespaces = namespace_to_common_keys.keys()

    out.write(f'export type Namespace =\n  {string_type(namespaces, indent_size=2)};\n\n')

    out.write('export interface NamespaceToKeys {\n')
    for namespace, keys in namespace_to_common_keys.items():
        key_type = string_type(keys, indent_size=4)
        out.write(f'  {quote}{namespace}{quote}:\n    {key_type};\n\n')
    out.write('}')


def find_common_keys(namespace, locales_dir):
    lang_to_keys = {}
    for ns_file_path in glob.glob(os.path.join(locales_dir, '*', f'{namespace}.yaml')):
        lang = os.path.relpath(ns_file_path, start=locales_dir).split(os.sep)[0]

        with open(ns_file_path, 'r') as ns_file:
            ns_dict = yaml.load(ns_file, Loader=yaml.SafeLoader)

        lang_to_keys[lang] = set(get_paths(ns_dict))

    return set.intersection(*lang_to_keys.values())


def find_namespaces(locales_dir):
    namespaces = set()
    for lang in os.listdir(locales_dir):
        lang_dir = os.path.join(locales_dir, lang)
        for yaml_path in glob.glob(os.path.join(locales_dir, lang, '**', '*.yaml'), recursive=True):
            ns = os.path.relpath(yaml_path, lang_dir).replace('.yaml', '')
            namespaces.add(ns)

    return namespaces


def get_paths(d: dict):
    for key, value in d.items():
        if isinstance(value, dict):
            for nested_path in get_paths(value):
                yield f'{key}.{nested_path}'
        else:
            yield key


if __name__ == '__main__':
    main()
