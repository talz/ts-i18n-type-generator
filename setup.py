import setuptools

setuptools.setup(
    name='generate_i18n_ts_types',
    version='0.0.1',
    packages=['generate_i18n_ts_types'],
    install_requires=[],
    entry_points={
        'console_scripts': [
            'generate-i18n-ts-types = generate_i18n_ts_types:main',
        ],
    },
)
